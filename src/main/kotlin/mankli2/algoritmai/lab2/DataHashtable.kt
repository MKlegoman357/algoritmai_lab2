package mankli2.algoritmai.lab2

class DataHashtable<T>(initialArrayLength: Int = 100) : Iterable<T> {
    var size: Int = 0
        private set

    private var array: Array<Any?> = arrayOfNulls(initialArrayLength)

    fun insert(value: T) {
        val index = hash(value)

        if (index == -1)
            throw IndexOutOfBoundsException(index)

        array[index] = value
        size++
    }

    fun search(value: T): Boolean {
        return array[hash(value)] != null
    }

    private fun hash(value: T): Int {
        val hash = Math.abs(value.hashCode()) % array.size
        var index = hash

        for (i in 0 until array.size) {
            val arrayValue = array[index]
            if (arrayValue == null || arrayValue == value) {
                return index
            }
            index = (hash + i * i) % array.size
        }

        return -1
    }

    override fun iterator(): Iterator<T> {
        return object : Iterator<T> {
            private var index: Int = -1

            init {
                findNextItem()
            }

            override fun hasNext(): Boolean {
                return index < array.size
            }

            override fun next(): T {
                val item = array[index]!!
                findNextItem()
                @Suppress("UNCHECKED_CAST")
                return item as T
            }

            private fun findNextItem() {
                index++

                for (i in index until array.size) {
                    if (array[i] != null) {
                        index = i
                        return
                    }
                }

                index = array.size
            }
        }
    }
}
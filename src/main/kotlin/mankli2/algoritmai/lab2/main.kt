package mankli2.algoritmai.lab2

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

typealias Generator<T> = (i: Int) -> T

infix fun <T> Generator<T>.withStep(step: Int): Generator<T> = { i -> this(i * step) }
infix fun <T> Generator<T>.start(start: Int): Generator<T> = { i -> this(start + i) }

fun main() {
    val linearGenerator: Generator<Int> = { i -> i }

    val amount = 8
    val iterations = 3
    val start = 10
    val step = 1

    val processorCount = 4
    val context = newFixedThreadPoolContext(processorCount, "thread pool")

    val names = loadNameList()
    val similarNameCount = 10

    val nameGenerator: Generator<List<String>> = { List(it) { names.random() } }
    val nameAmount = 10
    val nameStart = 1000000
    val nameStep = 1000000
    val hashtable = DataHashtable<String>((names.size * 1.5).toInt()).apply {
        for (name in names) insert(name)
    }

    testRecursiveAndDynamic(amount, iterations, linearGenerator start start withStep step)
    testRecursiveAndParallel(context, amount, iterations, linearGenerator start start withStep step)
    testSearchSimpleAndParallel(
        context,
        processorCount,
        nameAmount,
        iterations,
        nameGenerator start nameStart withStep nameStep,
        hashtable
    )

    do {
        print("Enter a name: ")
        val name = readLine()

        if (name != null && name != "q") {
            test("searching for names", 1, iterations, { name!! }) {
                findMostSimilar(it, names, similarNameCount)
            }
        }
    } while (name != null && name != "q")

    /*test("recursive", amount, iterations, linearGenerator start start withStep step) {
        recursive(it)
    }*/

    /*test("dynamic", amount, iterations, linearGenerator start start withStep step) {
        dynamic(it)
    }*/

    /*test("parallel recursive", amount, iterations, linearGenerator start start withStep step) {
        parallelRecursive(it, context)
    }

    test("parallel recursive 2", amount, iterations, linearGenerator start start withStep step) {
        parallelRecursive2(it, processorCount, context)
    }*/

    /*test("search simple", 1, iterations, { randomItems }, { it.size }) {
        countContains(it, hashtable)
    }

    test("search parallel", 1, iterations, { randomItems }, { it.size }) {
        countContainsParallel(it, hashtable, context, processorCount)
    }*/
}

/** RECURSIVE AND DYNAMIC FUNCTIONS **/

fun recursive(n: Int): Long {
    if (n <= 0) return 1L

    var sum = 0L
    for (i in 0 until n) {
        sum += recursive(i) * recursive(n - 1 - i)
    }

    return sum
}

fun dynamic(n: Int): Long {
    val c = LongArray(n + 1)

    c[0] = 1

    for (j in 1..n) {
        for (i in 0 until j) {
            c[j] += c[i] * c[j - 1 - i]
        }
    }

    return c[n]
}

/** PARALLEL RECURSIVE FUNCTIONS **/

fun parallelRecursive(n: Int, context: CoroutineDispatcher): Long {
    if (n <= 0) return 1L

    val tasks = mutableListOf<Deferred<Long>>()
    for (i in 0 until n) {
        tasks.add(GlobalScope.async(context) {
            recursive(i) * recursive(n - 1 - i)
        })
    }

    var sum = 0L
    runBlocking {
        for (task in tasks) {
            sum += task.await()
        }
    }

    return sum
}

fun parallelRecursive2(n: Int, processorCount: Int, context: CoroutineDispatcher): Long {
    if (n <= 0) return 1L

    val tasks = mutableListOf<Deferred<Long>>()
    for (j in 0 until processorCount) {
        tasks.add(GlobalScope.async(context) {
            var sum = 0L
            var i = j
            while (i < n) {
                sum += recursive(i) * recursive(n - 1 - i)
                i += processorCount
            }
            sum
        })
    }

    var sum = 0L
    runBlocking {
        for (task in tasks) {
            sum += task.await()
        }
    }

    return sum
}

/** SEARCHING FUNCTIONS **/

fun <T> countContains(items: List<T>, hashtable: DataHashtable<T>): Int {
    var count = 0

    for (item in items) {
        if (hashtable.search(item)) {
            count++
        }
    }

    return count
}

fun <T> countContainsParallel(
    items: List<T>,
    hashtable: DataHashtable<T>,
    context: CoroutineDispatcher,
    processorCount: Int
): Int {
    val partLength = items.size / processorCount
    val tasks: Array<Deferred<Int>?> = arrayOfNulls(processorCount)
    for (j in 0 until processorCount) {
        tasks[j] = GlobalScope.async(context) {
            val upperBound = if (j == processorCount - 1) items.size else (j + 1) * partLength
            var partialCount = 0

            for (i in j * partLength until upperBound) {
                if (hashtable.search(items[i])) {
                    partialCount++
                }
            }

            partialCount
        }
    }

    var count = 0
    runBlocking {
        for (task in tasks) {
            count += task!!.await()
        }
    }

    return count
}

/** LONGEST COMMON SUBSEQUENCE FUNCTIONS **/

/**
 * http://index-of.co.uk/Algorithms/Introduction%20to%20Algorithms%203rd%20Edition%20Sep%202010.pdf (page 394)
 */
fun lcsLength(a: String, b: String): Int {
    val aLength = a.length
    val bLength = b.length
    val lengths = Array(aLength + 1) { IntArray(bLength + 1) }

    for (i in 1..aLength) {
        for (j in 1..bLength) {
            lengths[i][j] = when {
                a[i - 1] == b[j - 1] -> lengths[i - 1][j - 1] + 1
                lengths[i - 1][j] >= lengths[i][j - 1] -> lengths[i - 1][j]
                else -> lengths[i][j - 1]
            }
        }
    }

    return lengths[aLength][bLength]
}

fun findMostSimilar(str: String, list: Iterable<String>, amount: Int = 10): List<String> {
    val mostSimilar = TreeSet<Pair<String, Int>>(
        compareByDescending<Pair<String, Int>> { it.second }.thenBy { it.first }
    )

    for (item in list) {
        val similarity = lcsLength(str.toLowerCase(), item.toLowerCase())

        if (mostSimilar.size < amount) {
            mostSimilar.add(item to similarity)
        } else {
            val last = mostSimilar.last()

            if (last.second < similarity) {
                mostSimilar.remove(last)
                mostSimilar.add(item to similarity)
            }
        }
    }

    return mostSimilar.map { it.first }
}

/** TESTING FUNCTIONS **/

fun <T, R> test(
    name: String,
    amount: Int,
    iterations: Int,
    dataGenerator: Generator<T>,
    dataFormatter: ((data: T) -> Any)? = null,
    func: (data: T) -> R
) {
    println("Running $name ($iterations iterations in each test):")
    println(" #  | data       | duration (ms) | results")

    for (i in 0 until amount) {
        printf("%3d | ", i + 1)

        val data = dataGenerator(i)

        printf("%10s | ", dataFormatter?.invoke(data) ?: data)

        var start = System.nanoTime()
        val result = func(data)
        var duration = System.nanoTime() - start

        for (j in 1 until iterations) {
            start = System.nanoTime()
            func(data)
            duration += System.nanoTime() - start
        }

        duration /= iterations

        printfln("%,13.3f | %s", duration / 1000000f, result)
    }

    println()
}

fun testRecursiveAndDynamic(amount: Int, iterations: Int, dataGenerator: Generator<Int>) {
    test("recursive", amount, iterations, dataGenerator) {
        recursive(it)
    }

    test("dynamic", amount, iterations, dataGenerator) {
        dynamic(it)
    }
}

fun testRecursiveAndParallel(
    context: CoroutineDispatcher,
    amount: Int,
    iterations: Int,
    dataGenerator: Generator<Int>
) {
    test("recursive", amount, iterations, dataGenerator) {
        recursive(it)
    }

    test("parallel recursive", amount, iterations, dataGenerator) {
        parallelRecursive(it, context)
    }
}

fun <T> testSearchSimpleAndParallel(
    context: CoroutineDispatcher,
    processorCount: Int,
    amount: Int,
    iterations: Int,
    generator: Generator<List<T>>,
    hashtable: DataHashtable<T>
) {
    test("search simple", amount, iterations, generator, { it.size }) {
        countContains(it, hashtable)
    }

    test("search parallel", amount, iterations, generator, { it.size }) {
        countContainsParallel(it, hashtable, context, processorCount)
    }
}

/** NAME LOADING FUNCTIONS **/

fun loadNameList(path: String = "names"): List<String> {
    val classLoader = Thread.currentThread().contextClassLoader
    val files = BufferedReader(InputStreamReader(classLoader.getResourceAsStream(path))).readLines()
    val names = mutableListOf<String>()

    for (file in files) {
        if (file.endsWith(".json")) {
            names.addAll(
                Gson().fromJson(
                    InputStreamReader(classLoader.getResourceAsStream("$path/$file")),
                    object : TypeToken<List<String>>() {}.type
                )
            )
        }
    }

    return names
}

/** UTILITY FUNCTIONS **/

fun printf(format: String, vararg args: Any?) {
    print(format.format(*args))
}

fun printfln(format: String, vararg args: Any?) {
    println(format.format(*args))
}